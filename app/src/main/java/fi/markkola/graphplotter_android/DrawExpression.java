/* DrawExpression.java
 *
 * Copyright (C) 2016 Paavo Markkola <paavo.markkola@iki.fi>
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE.txt file for details.
 */
package fi.markkola.graphplotter_android;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import fi.markkola.calculator.expression.EvaluationErrorException;
import fi.markkola.calculator.expression.PostfixSolver;
import fi.markkola.calculator.graph.Graph;
import fi.markkola.calculator.graph.GraphLimits;
import fi.markkola.calculator.graph.GraphPoint;

import fi.markkola.graphplotter_android.graphpanel.GraphPanel;

/**
 * Activity that draws graphs from expressions to a graph panel.
 *
 * @author Paavo Markkola
 */
public class DrawExpression extends AppCompatActivity {
    /**
     * View used to display a graph.
     */
    GraphPanel graphPanel;

    /**
     * Layouts for DrawExpression actions.
     */
    LinearLayout layoutPortrait;
    LinearLayout layoutLandscape;

    /**
     * Options menu for DrawExpression activity.
     */
    Menu optionsMenu;

    /**
     * Layout for cursor controls. Menu option show cursor is used to hide or
     * show this layout.
     */
    LinearLayout cursorLayout;

    /**
     * Listener for changes in layout size. Needed for expanding the graph
     * panel to maximum size while keeping square shape.
     */
    View.OnLayoutChangeListener layoutChangeListener;

    /**
     * Dialog for indicating evaluation errors to the user.
     */
    private AlertDialog evaluationErrorDialog;

    /**
     * Default size for the graph panel.
     */
    private int graphPanelWidth = 500;
    private int graphPanelHeight = 500;

    /**
     * The graph as mathematical expression as provided by the user. Must be
     * set and be valid for the graph to be drawn.
     */
    private String expression;

    /**
     * PostfixSolver instance that is used to calculate values for each point
     * on the graph.
     */
    private PostfixSolver postfixSolver;

    /**
     * The class that handles building of the graph from the expression string.
     */
    private Graph graph;

    /**
     * Minimum and maximum limits on x and y axes for the graph. All possible
     * limits ranges are kept in a list. Currently used limits are referenced
     * from this list. This allows for zoom levels.
     */
    private GraphLimits currentLimits;
    private List<GraphLimits> graphLimits;

    /**
     * When cursor is enable this point contains the location of the cursor.
     */
    private GraphPoint cursorPoint;

    /**
     * Index of the graph where cursor is currently located.
     */
    private int cursorIndex;

    /**
     * Boolean indicating whether cursor is to be displayed or not.
     */
    private boolean cursorShow = false;

    /**
     * Boolean indicating whether the graph needs to be recalculated or not.
     * Recalculation is needed for example when limits are changed.
     */
    private boolean graphUpdateNeeded;

    /**
     * Zoom level constant for minimum, maximum and default.
     */
    private final static int ZOOM_LEVEL_DEFAULT = 5;
    private final static int ZOOM_LEVEL_MIN = 1;
    private final static int ZOOM_LEVEL_MAX = 7;

    /**
     * Current level of zoom. Zooming in decreases and zooming out increases
     * the zoom level.
     */
    private int zoomLevel = ZOOM_LEVEL_DEFAULT;

    /**
     * Override onCreate
     *
     * @param savedInstanceState    Previous state.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_draw_expression);
        findRequiredViews();
        initLayoutChangeListener();
        initGraph();
        retrieveExpressionString();
        drawGraph();
        resetCursor();
        updateCursorPoint();
        initGraphPanelTouch();
    }

    /**
     * Inflate appropriate menu for this activity.
     *
     * @param menu  Menu to be inflated.
     * @return      True for displaying the menu.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.draw_expression, menu);
        optionsMenu = menu;
        return true;
    }

    /**
     * Handle options menu items.
     *
     * @param item  Menu item selected byt the user.
     * @return      True for handling the item.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.new_expression:
                handleNewExpressionMenuItem();
                return true;
            case R.id.show_cursor:
                handleShowCursorMenuItem(item);
                return true;
            case R.id.reset_cursor:
                handleResetCursorMenuItem();
                return true;
            case R.id.zoom_in:
                handleZoomInMenuItem();
                return true;
            case R.id.zoom_out:
                handleZoomOutMenuItem();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Handle new expression in options menu. Change current activity to
     * activity_set_expression.
     */
    private void handleNewExpressionMenuItem() {
        Intent intent = new Intent(this, SetExpression.class);
        startActivity(intent);
    }

    /**
     * Handle show cursor item in options menu. Change the checkboc status and
     * show or hide cursor accordingly.
     *
     * @param item    Show cursor menu item.
     */
    private void handleShowCursorMenuItem(MenuItem item) {
        boolean status = !item.isChecked();
        setShowCursor(status);
        item.setChecked(status);
        optionsMenu.findItem(R.id.reset_cursor).setVisible(status);
        updateCursorPoint();
    }

    /**
     * Handle reset cursor in options menu.
     */
    private void handleResetCursorMenuItem() {
        resetCursor();
        updateCursorPoint();
    }

    /**
     * Handle zoom in in options menu.
     */
    private void handleZoomInMenuItem() {
        if (zoomLevel > ZOOM_LEVEL_MIN) {
            zoomLevel--;
            updateZoomLevel();
        }
    }

    /**
     * Handle zoom out in options menu.
     */
    private void handleZoomOutMenuItem() {
        if (zoomLevel < ZOOM_LEVEL_MAX) {
            zoomLevel++;
            updateZoomLevel();
        }
    }

    /**
     * Update zoom level for the graph panel. Set limits according to the
     * new zoom level. Graph must be redrawn and cursor reset as well.
     */
    private void updateZoomLevel() {
        currentLimits = graphLimits.get(zoomLevel);
        graphPanel.setLimits(currentLimits.getMinX(), currentLimits.getMaxX(),
                             currentLimits.getMinY(), currentLimits.getMaxY());
        graphPanel.setCrossLinesCount(zoomLevel * 2);
        drawGraph();
        resetCursor();
        updateCursorPoint();
    }

    /**
     * Get the expression string from intent.
     */
    private void retrieveExpressionString() {
        setExpression(getIntent().getStringExtra(SetExpression.EXPRESSION));
    }

    /**
     * Finds views required for this action by their Id.
     */
    private void findRequiredViews() {
        layoutPortrait =
                (LinearLayout) findViewById(R.id.drawExpressionLayoutPortrait);
        layoutLandscape =
                (LinearLayout) findViewById(R.id.drawExpressionLayoutLandscape);
        graphPanel = (GraphPanel) findViewById(R.id.GraphPanelView);
        cursorLayout = (LinearLayout) findViewById(R.id.cursorPositionLayout);
    }

    /**
     * Initialize listener for layout size changes.
     */
    private void initLayoutChangeListener() {
        createLayoutChangeListener();
        if (getResources().getConfiguration().
                            orientation == Configuration.ORIENTATION_PORTRAIT)
            layoutPortrait.addOnLayoutChangeListener(layoutChangeListener);
        else
            layoutLandscape.addOnLayoutChangeListener(layoutChangeListener);
    }

    /**
     * Create new OnLayoutChangeListener for updating GraphPanel instance if
     * layout size has changed.
     */
    private void createLayoutChangeListener() {
        layoutChangeListener = new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right,
                                       int bottom, int oldLeft, int oldTop,
                                       int oldRight, int oldBottom) {
                handleLayoutSizeChanged(v);
            }
        };
    }

    /**
     * Find out whether width or height defines the size of the square shaped
     * GraphPanel instance and check if this parameter has changed. Update the
     * size of the panel if necessary.
     *
     * @param layout    Layout from which size parameter is to be retrieved.
     */
    private void handleLayoutSizeChanged(View layout) {
        if (getResources().getConfiguration().orientation ==
                Configuration.ORIENTATION_PORTRAIT) {
            if (graphPanelWidth != layout.getWidth()) {
                updateGraphPanelSize(layout.getWidth());
            }
        } else {
            if (graphPanelHeight != layout.getHeight()) {
                updateGraphPanelSize(layout.getHeight());
            }
        }
    }

    /**
     * Update size of the GraphPanel instance and redraw everything.
     *
     * @param size      Width and length of the panel.
     */
    private void updateGraphPanelSize(int size) {
        setGraphPanelSize(size);
        graphPanel.setPanelSize(graphPanelWidth, graphPanelHeight);
        graphPanel.setOrigo(graphPanelWidth / 2, graphPanelHeight / 2);
        drawGraph();
        resetCursor();
        updateCursorPoint();
    }

    /**
     * Ensure that the dimension of a GraphPanel instance form a square by
     * setting panel width and height to the same value.
     *
     * @param size     Width and length of the panel.
     */
    private void setGraphPanelSize(int size) {
        graphPanelWidth = size;
        graphPanelHeight = size;
    }

    /**
     * Initialize graph with default values.
     */
    private void initGraph() {
        setExpression(null);
        setPostfixCalculator();
        setGraphLimits();
        setDefaultLimits();
        setGraph();
        setGraphPoints();
    }

    /**
     * Set the string given as parameter as the expression for the graph.
     *
     * @param expression	Mathematical expression for the graph as a string.
     */
    private void setExpression(String expression) {
        this.expression = expression;
    }

    /**
     * Parse expression string, if it has been set and is not an empty string,
     * using configured PostfixSolver instance. Create new PostfixSolver
     * instance in case it does not yet exist.
     */
    private void setPostfixCalculator() {
        if (postfixSolver == null)
            postfixSolver = new PostfixSolver();
        if (expression != null && !expression.isEmpty())
            postfixSolver.parseExpression(expression);
    }

    /**
     * Set default limits for the graph.
     */
    private void setGraphLimits() {
        graphLimits = new ArrayList<>(ZOOM_LEVEL_MAX + 1);
        for (int index = 0; index < ZOOM_LEVEL_MAX + 1; index++) {
            GraphLimits limits = new GraphLimits();
            limits.setDefaultLimits();
            limits.setLimitsForX(-1 * index, index);
            limits.setLimitsForY(-1 * index, index);
            graphLimits.add(limits);
        }
    }

    /**
     * Restores coordinate limits back to default values for both x and y axes.
     */
    private void setDefaultLimits() {
        zoomLevel = ZOOM_LEVEL_DEFAULT;
        currentLimits = graphLimits.get(ZOOM_LEVEL_DEFAULT);
    }

    /**
     * Configure a new instance of Graph class. Graph is set to null
     * if expression string has not been set or is empty.
     */
    private void setGraph() {
        if (expression == null || expression.isEmpty())
            graph = null;
        else {
            graph = new Graph();
            graph.setExpressionSolver(postfixSolver);
            setGraphDimensions();
            setGraphUpdate(true);
        }
    }

    /**
     * Set dimensions for the Graph object. This includes limits and number of
     * discrete points on x and y axes.
     */
    private void setGraphDimensions() {
        if ((graph != null) && (currentLimits != null)) {
            graph.setGraphLimits(currentLimits);
            graph.setNumberOfPointsForX(graphPanelWidth);
            graph.setNumberOfPointsForY(graphPanelHeight);
        }
    }

    /**
     * Update the list of coordinate points representing a graph. New value
     * for each point is calculated only if update was requested earlier.
     */
    private void setGraphPoints() {
        if (graph != null) {
            if (graphUpdateNeeded)
                try {
                    graph.setGraph();
                } catch (EvaluationErrorException exception) {
                    graph = null;
                    showEvaluationErrorDialog(
                            getString(R.string.evaluation_error_title),
                            exception.getMessage());
                }
            setGraphUpdate(false);
        }
    }

    /**
     * Show a dialog when expression entered by the user is invalid.
     *
     * @param title     Title for the AlertDialog
     * @param message   Message for the AlertDialog
     */
    private void showEvaluationErrorDialog(String title, String message) {
        createEvaluationErrorDialog(title, message);
        evaluationErrorDialog.show();
    }

    /**
     * Create a new AlertDialog for displaying invalid expression error.
     *
     * @param title     Title for the AlertDialog
     * @param message   Message for the AlertDialog
     */
    private void createEvaluationErrorDialog(String title, String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setPositiveButton(R.string.evaluation_error_button,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(DrawExpression.this,
                                                   SetExpression.class);
                        startActivity(intent);
                    }
                });
        evaluationErrorDialog = alertDialogBuilder.create();
    }

    /**
     * Set whether graph should be updated and recalculated at next call to
     * paintComponent.
     *
     * @param update	True if the graph needs an update, false otherwise
     */
    private void setGraphUpdate(boolean update) {
        graphUpdateNeeded = update;
    }

    /**
     * Draws a new graph according to the mathematical expression stored in
     * field expression.
     */
    private void drawGraph() {
        setGraphUpdate(true);
        setExpression(expression);
        setPostfixCalculator();
        setGraph();
        setGraphDimensions();
        setGraphPoints();
        sendExpression();
        sendGraph();
    }
    /**
     * Send the expression string to the GraphPanel instance.
     */
    private void sendExpression() {
        graphPanel.setExpression(expression);
    }

    /**
     * Update the graph to the GraphPanel instance. First, clear any existing
     * graph that the panel might use and then go through the graph one point
     * at a time to update graph. When done send the graph to the panel.
     */
    private void sendGraph() {
        if (graphPanel == null)
            return;
        graphPanel.clearGraph();
        if (graph != null)
            for (GraphPoint point : graph.getGraph())
                if (point.yPixel > graphPanelHeight)
                    graphPanel.addGraphPoint(point.xPixel, point.yPixel);
                else
                    graphPanel.addGraphPoint(point.xPixel,
                                             graphPanelHeight - point.yPixel);
        graphPanel.updateGraph();
    }

    /**
     * Set cursor point if the graph is properly set.
     */
    private void setCursorPoint() {
        if ((graph == null) || (graph.getGraph() == null))
            return;
        if (cursorIndex == -1)
            cursorIndex = graph.getZeroIndex();
        if (isValidCursorIndex(cursorIndex))
            cursorPoint = graph.getGraph().get(cursorIndex);
        else
            cursorPoint = null;
    }

    /**
     * Determine if the point on a graph at given index is a valid cursor
     * point. For the point to be a valid cursor point, the graph must have
     * been properly set, the index must fall within the graph and the point
     * must be visible.
     *
     * @param index		Index of the point on a graph to be checked.
     */
    private boolean isValidCursorIndex(int index) {
        if ((graph == null) || (graph.getGraph() == null))
            return false;
        if ((index < 0) || (index >= graph.getGraph().size()))
            return false;
        GraphPoint point = graph.getGraph().get(index);
        return ((point.xPixel > 0) && (point.yPixel > 0) &&
                (point.xPixel < graphPanelWidth) &&
                (point.yPixel < graphPanelHeight));
    }

    /**
     * Set or clear cursor point from the GraphPanel instance.
     */
    private void setGraphPanelCursorPoint() {
        if ((cursorShow) && (cursorPoint != null))
            graphPanel.setCursor(cursorPoint.xPixel,
                    Math.abs(cursorPoint.yPixel - graphPanelHeight));
        else
            graphPanel.setCursor(Integer.MAX_VALUE, Integer.MAX_VALUE);
    }

    /**
     * Show or hide cursor.
     *
     * @param show	True if cursor is to be shown and false if not
     */
    private void setShowCursor(boolean show) {
        cursorShow = show;
        if (cursorShow)
            cursorLayout.setVisibility(View.VISIBLE);
        else
            cursorLayout.setVisibility(View.INVISIBLE);
    }

    /**
     * Reset cursor position.
     */
    private void resetCursor() {
        cursorIndex = -1;
        cursorPoint = null;
    }

    /**
     * Moves cursor to left by one point along the graph. If new position for
     * the cursor is not valid, i.e., the position does not exist, then finds
     * the next valid position on graph. If there is no valid position on the
     * left then cursor will remain on the old position.
     */
    public void moveCursorToLeft(View view) {
        if ((!cursorShow) || (cursorIndex == -1))
            return;
        for (int index = cursorIndex - 1; index >= 0; index--) {
            if (isValidCursorIndex(index)) {
                cursorIndex = index;
                cursorPoint = graph.getGraph().get(cursorIndex);
                break;
            }
        }
        updateCursorPoint();
    }

    /**
     * Moves cursor to right by one point along the graph. If new position for
     * the cursor is not valid, i.e., the position does not exist, then finds
     * the next valid position on graph. If there is no valid position on the
     * right then cursor will remain on the old position.
     */
    public void moveCursorToRight(View view) {
        if ((!cursorShow) || (cursorIndex == -1))
            return;
        for (int index = cursorIndex + 1;
             index < graph.getGraph().size();
             index++) {
            if (isValidCursorIndex(index)) {
                cursorIndex = index;
                cursorPoint = graph.getGraph().get(cursorIndex);
                break;
            }
        }
        updateCursorPoint();
    }

    /**
     * Set values of cursor point to their respective label for user to see.
     */
    private void setCursorValues() {
        TextView labelPositionX = (TextView) findViewById(R.id.labelPositionX);
        TextView labelPositionY = (TextView) findViewById(R.id.labelPositionY);
        if (cursorPoint == null) {
            labelPositionX.setText(getString(R.string.cursor_position_x));
            labelPositionY.setText(getString(R.string.cursor_position_y));
        } else {
            DecimalFormat positionFormat = new DecimalFormat("###0.0000");
            labelPositionX.setText(String.format(Locale.ROOT, "%s %s",
                                   getString(R.string.cursor_position_x),
                                   positionFormat.format(cursorPoint.x)));
            labelPositionY.setText(String.format(Locale.ROOT, "%s %s",
                                   getString(R.string.cursor_position_y),
                                   positionFormat.format(cursorPoint.y)));
        }
    }

    /**
     * Update the position of the cursor. Do this for both the action and the
     * custom view component GraphPanel. Also update labels showing x, y values
     * of the cursor point.
     */
    private void updateCursorPoint() {
        setCursorPoint();
        setGraphPanelCursorPoint();
        setCursorValues();
    }

    /**
     * Listen for touch events on the graph panel and change cursor position
     * accordingly if cursor has been enabled.
     */
    private void initGraphPanelTouch() {
        GraphPanel view = (GraphPanel) findViewById(R.id.GraphPanelView);
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (!cursorShow)
                    return false;
                if (event.getAction() == MotionEvent.ACTION_DOWN)
                    return true;
                if (event.getAction() == MotionEvent.ACTION_UP){
                    cursorIndex = (int) event.getAxisValue(0);
                    updateCursorPoint();
                    return true;
                }
                return false;
            }
        });
    }
}

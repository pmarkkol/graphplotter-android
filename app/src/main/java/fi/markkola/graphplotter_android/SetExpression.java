/* SetExpression.java
 *
 * Copyright (C) 2016 Paavo Markkola <paavo.markkola@iki.fi>
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE.txt file for details.
 */
package fi.markkola.graphplotter_android;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.TextView;

import fi.markkola.calculator.expression.ExpressionSolver;
import fi.markkola.calculator.expression.InvalidExpressionException;
import fi.markkola.calculator.expression.PostfixSolver;

/**
 * Activity that reads expression from user input, validates it and
 * forwards it to the activity that actually draws for the user to see.
 *
 * @author Paavo Markkola
 */
public class SetExpression extends AppCompatActivity {
    /**
     * Limit maximum length of expression strings to 32 characters.
     */
    private static final int MAX_EXPRESSION_LENGTH = 32;

    /**
     * Identity for transferring expression string to the draw expression
     * activity.
     */
    public final static String EXPRESSION =
                                "fi.markkola.graphplotter_android.EXPRESSION";

    /**
     * Options menu for SetExpression activity.
     */
    Menu optionsMenu;

    /**
     * Text field for user to enter expressions.
     */
    private EditText textFieldExpression;

    /**
     * ExpressionSolver instance to parse and validate the expression.
     */
    private ExpressionSolver expressionSolver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_expression);

        textFieldExpression = (EditText) findViewById(R.id.textFieldExpression);

        initExpressionTextFieldAction();
        initExpressionSolver();
    }

    /**
     * Inflate appropriate menu for this activity.
     *
     * @param menu  Menu to be inflated.
     * @return      True for displaying the menu.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.set_expression, menu);
        optionsMenu = menu;
        return true;
    }

    /**
     * Handle options menu items.
     *
     * @param item  Menu item selected byt the user.
     * @return      True for handling the item.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.instructions:
                handleInstructionsMenuItem();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Handle instructions in options menu.
     */
    private void handleInstructionsMenuItem() {
        showAlertDialogHTML(
                getString(R.string.user_instructions_title),
                String.format(getString(R.string.user_instructions_template),
                              expressionSolver.getUserInstructions()));
    }

    /**
     * Set expression text field to trigger draw action when enter key is
     * pressed.
     */
    private void initExpressionTextFieldAction() {
        textFieldExpression.setOnEditorActionListener(
                                        new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND ||
                    actionId == EditorInfo.IME_ACTION_UNSPECIFIED) {
                    drawExpression(v);
                }
                return false;
            }
        });
    }

    /**
     * Initialize new PostfixSolver to parse given expressions.
     */
    private void initExpressionSolver() {
        expressionSolver = new PostfixSolver();
    }

    /**
     * Check whether given expression string is a valid expression. The string
     * must not be empty and its length must be within allowed limit. Also, the
     * expression must pass parsing.
     *
     * @param expression    Expression to be validated as string.
     * @return              True if expression is valid, false otherwise.
     */
    private boolean isValidExpression(String expression) {
        if (expression.isEmpty() ||
            expression.length() > MAX_EXPRESSION_LENGTH) {
            showAlertDialogHTML(
                 getString(R.string.invalid_expression_title),
                 String.format(getString(R.string.invalid_expression_template),
                               getString(R.string.invalid_expression_message)));
            return false;
        }
        try {
            expressionSolver.parseExpression(expression);
        } catch (InvalidExpressionException exception) {
            showInvalidExpressionExceptionDialog(exception);
            return false;
        }
        return true;
    }

    /**
     * Show a dialog for invalid expression exception.
     *
     * @param exception     Exception received.
     */
    private void showInvalidExpressionExceptionDialog(
                                        InvalidExpressionException exception) {
        String message, spaces = "";
        for (int i = 0; i < exception.getPosition(); i++)
            spaces += "&nbsp;";
        if (exception.getExpression().isEmpty())
            message =
                  String.format(getString(R.string.invalid_expression_template),
                                exception.getMessage());
        else
            message = String.format(
                    getString(R.string.invalid_expression_exception_template),
                    exception.getMessage(), exception.getExpression(), spaces);
        showAlertDialogHTML(getString(R.string.invalid_expression_title),
                            message);
    }

    /**
     * Create a new AlertDialog for displaying user instructions.
     *
     * @param title     Title for the AlertDialog
     * @param content   Content for the AlertDialog
     */
    private void showAlertDialogHTML(String title, String content) {
        WebView webView = new WebView(this);
        webView.loadData(content, "text/html", "utf-8");
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setView(webView);
        alertDialogBuilder.setPositiveButton(R.string.dialog_close_button,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) { }
                });
        alertDialogBuilder.create().show();
    }

    /**
     * Called when button to draw an expression was clicked. Reads the
     * expression from the expression text field, validates the string and
     * changes activity to plot to the graph. Expression must not be empty or
     * longer than MAX_EXPRESSION_LENGTH for it to be valid.
     *
     * @param view  Button that was clicked.
     */
    public void drawExpression(View view) {
        String expression = textFieldExpression.getText().toString();
        if (isValidExpression(expression)) {
            Intent intent = new Intent(this, DrawExpression.class);
            intent.putExtra(EXPRESSION, expression);
            startActivity(intent);
        }
    }

    /**
     * Called when button to clear expression from the text field is clicked.
     * Sets empty string to the expression text field prompting hint to be
     * shown.
     *
     * @param view    Button that was clicked.
     */
    public void clearExpression(View view) {
        textFieldExpression.setText(R.string.expression_empty);
    }
}

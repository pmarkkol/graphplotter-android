/* GraphPanelBorder.java
 *
 * Copyright (C) 2016 Paavo Markkola <paavo.markkola@iki.fi>
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE.txt file for details.
 */
package fi.markkola.graphplotter_android.graphpanel;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

/**
 * Class for creating a border to a graph panel.
 *
 * @author Paavo Markkola
 */
public class GraphPanelBorder {

    private int width;
    private int height;
    private Paint borderPaint;
    private Rect borderRectangle;

    public GraphPanelBorder() {
        borderRectangle = new Rect();
        borderPaint = new Paint();
        borderPaint.setColor(Color.BLACK);
        borderPaint.setStyle(Paint.Style.STROKE);
        borderPaint.setStrokeWidth(2);
    }

    public void draw(Canvas canvas) {
        width = canvas.getWidth();
        height = canvas.getHeight();

        borderRectangle.set(0, 0, width, height);
        canvas.drawRect(borderRectangle, borderPaint);
    }
}

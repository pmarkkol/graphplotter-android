/* GraphPanelLine.java
 *
 * Copyright (C) 2016 Paavo Markkola <paavo.markkola@iki.fi>
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE.txt file for details.
 */
package fi.markkola.graphplotter_android.graphpanel;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;

/**
 * Class that represents a line on a graph panel.
 *
 * @author Paavo Markkola
 */
public class GraphPanelLine {

    /**
     * Boolean that determines of the line is to be visible or not.
     */
    private boolean visible;

    /**
     * String and ending points of the line.
     */
    private GraphPanelPoint start;
    private GraphPanelPoint end;

    /**
     * Paint for the line.
     */
    private Paint linePaint;

    /**
     * Construct a new graph line with no specific starting or ending point.
     */
    public GraphPanelLine() {
        visible = true;
        start = new GraphPanelPoint();
        end = new GraphPanelPoint();
        initLinePaint();
    }

    /**
     * Construct a new graph line with specific starting or ending points.
     *
     * @param start Point where line is to start.
     * @param end   Point where the line is to end.
     */
    public GraphPanelLine(GraphPanelPoint start, GraphPanelPoint end) {
        visible = true;
        this.start = start;
        this.end = end;
        initLinePaint();
    }

    /**
     * Initialize line paint with default settings.
     */
    private void initLinePaint() {
        linePaint = new Paint();
        linePaint.setColor(Color.BLACK);
        linePaint.setStrokeWidth(2);
    }

    /**
     * Set start point for the line.
     *
     * @param x     Horizontal coordinate of the start point.
     * @param y     Vertical coordinate of the start point.
     */
    public void setLineStartPoint(int x, int y) {
        start.setX(x);
        start.setY(y);
    }

    /**
     * Set end point for the line.
     *
     * @param x     Horizontal coordinate of the end point.
     * @param y     Vertical coordinate of the end point.
     */
    public void setLineEndPoint(int x, int y) {
        end.setX(x);
        end.setY(y);
    }

    /**
     * Set the line style as graph line, i.e., use blue color and normal stroke
     * with width of 2.
     */
    public void setStyleGraphLine() {
        linePaint.setColor(Color.BLUE);
        linePaint.setStrokeWidth(2);
        linePaint.setPathEffect(null);
    }

    /**
     * Set the line style as cross line, i.e., use grey color and dashed stroke
     * with width of 1.
     */
    public void setStyleCrossLine() {
        linePaint.setColor(Color.GRAY);
        linePaint.setStrokeWidth(1);
        linePaint.setPathEffect(new DashPathEffect(new float[] {10,10}, 0));
    }

    /**
     * Set the line style as origo line, i.e., use black color and normal
     * stroke with width of 2.
     */
    public void setStyleOrigoLine() {
        linePaint.setColor(Color.BLACK);
        linePaint.setStrokeWidth(2);
        linePaint.setPathEffect(null);
    }

    /**
     * Set the line style as cursor line, i.e., use red color and dashed stroke
     * with width of 1.
     */
    public void setStyleCursorLine() {
        linePaint.setColor(Color.RED);
        linePaint.setStrokeWidth(1);
        linePaint.setPathEffect(new DashPathEffect(new float[] {10,10}, 0));
    }

    /**
     * Set the line visible or invible.
     *
     * @param visible   True if line is to be visible, and false if not.
     */
    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    /**
     * Draw line the the canvas provided as parameter. Do not draw if the line
     * has not been set visible.
     *
     * @param canvas    Canvas on which the lines is to be drawn.
     */
    public void draw(Canvas canvas) {
        if (visible)
            canvas.drawLine(start.getX(), start.getY(),
                            end.getX(), end.getY(), linePaint);
    }
}

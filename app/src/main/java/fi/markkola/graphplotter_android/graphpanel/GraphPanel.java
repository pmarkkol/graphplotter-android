/* GraphPanel.java
 *
 * Copyright (C) 2016 Paavo Markkola <paavo.markkola@iki.fi>
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE.txt file for details.
 */
package fi.markkola.graphplotter_android.graphpanel;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Android View component that allows mathematical expressions to be drawn as
 * graphs.
 *
 * @author Paavo Markkola
 */
public class GraphPanel extends View {
    /**
     * Default spacing in pixels for text outputs.
     */
    private static final int DEFAULT_TEXT_SPACING = 5;

    /**
     * Default panel size is 500*500 pixels.
     */
    private static final int DEFAULT_PANEL_WIDTH = 500;
    private static final int DEFAULT_PANEL_HEIGHT = 500;

    /**
     * Default number of cross lines. Should be even so that both sides of
     * the origo line have the same number of cross lines.
     */
    private static final int DEFAULT_CROSS_LINES_COUNT = 10;

    /**
     * Panel size fields. Starts with the default ones but user may change
     * panel size at any time.
     */
    private int panelWidth = DEFAULT_PANEL_WIDTH;
    private int panelHeight = DEFAULT_PANEL_HEIGHT;

    /**
     * List of points that make up a graph.
     */
    private List<GraphPanelPoint> graph;
    private List<GraphPanelPoint> graphPointStore;

    /**
     * Minimum and maximum limits for both x and y axes.
     */
    private GraphPanelLimits limits;

    /**
     * Point where both x and y are zero.
     */
    private GraphPanelPoint origo;

    /**
     * Point where cursor lines cross.
     */
    private GraphPanelPoint cursor;

    /**
     * Vertical and horizontal lines that cross at the origo.
     */
    private GraphPanelLine horizontalOrigoLine;
    private GraphPanelLine verticalOrigoLine;

    /**
     * Group that holds lines connecting each point a graph.
     */
    private List<GraphPanelLine> graphLines;

    /**
     * List that holds cross lines.
     */
    private List<GraphPanelLine> crossLines;

    /**
     * Vertical and horizontal lines that cross at set cursor point.
     */
    private GraphPanelLine verticalCursorLine;
    private GraphPanelLine horizontalCursorLine;

    /**
     * The graph panel has a rectangle shaped border.
     */
    private GraphPanelBorder border;

    /**
     * GraphPanelText instance used to output expression string to graph panel.
     */
    private GraphPanelText expressionText;

    /**
     * GraphPanelText instances used to output limits of both x and y axes.
     */
    private GraphPanelText textLimitMinX;
    private GraphPanelText textLimitMaxX;
    private GraphPanelText textLimitMinY;
    private GraphPanelText textLimitMaxY;

    /**
     * String containing mathematical expression.
     */
    private String expression = "";

    /**
     * Defines number of crosslines used on both x and y axes.
     */
    private int crossLineCount = DEFAULT_CROSS_LINES_COUNT;


    public GraphPanel(Context context) {
        super(context);
        initGraphPanel();
    }

    public GraphPanel(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initGraphPanel();
    }

    /**
     * Initialize GraphPanel with default values.
     */
    private void initGraphPanel() {
        initGraphPanelBorder();
        initOrigo();
        initOrigoLines();
        initLimits();
        initCrossLines();
        initExpressionText();
        initGraphLines();
        initCursorLines();
    }

    /**
     * Initialize border for the panel.
     */
    private void initGraphPanelBorder() {
        border = new GraphPanelBorder();
    }

    /**
     * Initialize origo point. Set origo to center of the panel.
     */
    private void initOrigo() {
        origo = new GraphPanelPoint(DEFAULT_PANEL_WIDTH / 2,
                                    DEFAULT_PANEL_HEIGHT / 2);
    }

    /**
     * Initialize origo lines that are set to cross where x and y are both 0.
     */
    private void initOrigoLines() {
        horizontalOrigoLine = new GraphPanelLine();
        verticalOrigoLine = new GraphPanelLine();
    }

    /**
     * Initialize minimum and maximum limits for both x and y axes. Also
     * initialize text views that show the limits on the panel.
     */
    private void initLimits() {
        limits = new GraphPanelLimits();
        textLimitMinX = new GraphPanelText();
        textLimitMaxX = new GraphPanelText();
        textLimitMinY = new GraphPanelText();
        textLimitMaxY = new GraphPanelText();
    }

    /**
     * Initialize cross lines set. This is a collection that hold both vertical
     * and horizontal cross lines.
     */
    private void initCrossLines() {
        crossLines = new ArrayList<>();
    }

    /**
     * Initialize text view that is used to show expression as a string on the
     * panel.
     */
    private void initExpressionText() {
        expressionText = new GraphPanelText();
    }

    /**
     * Initialize list to hold lines that make up a graph.
     */
    private void initGraphLines() {
        graphLines = new ArrayList<>();
    }

    /**
     * Initialize vertical and horizontal lines that meet at point where cursor
     * is set.
     */
    private void initCursorLines() {
        verticalCursorLine = new GraphPanelLine();
        horizontalCursorLine = new GraphPanelLine();
    }

    /**
     * Update both vertical and horizontal origo lines.
     */
    private void setOrigoLines() {
        setVerticalOrigoLine();
        setHorizontalOrigoLine();
    }

    /**
     * Check if vertical origo line is set to be visible. If not remove it from
     * the panel. If yes, then update the location of the line.
     */
    private void setVerticalOrigoLine() {
        verticalOrigoLine.setVisible(false);
        if (origo.getX() >= 0 && origo.getX() < Integer.MAX_VALUE)
            verticalOrigoLine.setVisible(true);
        setVerticalOrigoLinePosition();
    }

    /**
     * Set vertical origo line according to the origo point.
     */
    private void setVerticalOrigoLinePosition() {
        verticalOrigoLine.setLineStartPoint(origo.getX(), 0);
        verticalOrigoLine.setLineEndPoint(origo.getX(), getHeight());
        verticalOrigoLine.setStyleOrigoLine();
    }

    /**
     * Check if horizontal origo line is set to be visible. If not remove it
     * from the panel. If yes, then update the location of the line.
     */
    private void setHorizontalOrigoLine() {
        horizontalOrigoLine.setVisible(false);
        if (origo.getY() > 0 && origo.getY() < Integer.MAX_VALUE)
            horizontalOrigoLine.setVisible(true);
        setHorizontalOrigoLinePosition();
    }

    /**
     * Set horizontal origo line according to the origo point.
     */
    private void setHorizontalOrigoLinePosition() {
        horizontalOrigoLine.setLineStartPoint(0, origo.getY());
        horizontalOrigoLine.setLineEndPoint(getWidth(), origo.getY());
        horizontalOrigoLine.setStyleOrigoLine();
    }

    /**
     * Remove cross lines from the panel and add them again. Use a collection
     * to store and handle the lines.
     */
    private void setCrossLines() {
        crossLines.clear();
        setVerticalCrossLines(getVerticalCrossLineSpacing());
        setHorizontalCrossLines(getHorizontalCrossLineSpacing());
        for (GraphPanelLine line : crossLines)
            line.setStyleCrossLine();
    }

    /**
     * Get spacing between vertical cross lines.
     *
     * @return	The number of pixels between vertical cross lines.
     */
    private int getVerticalCrossLineSpacing() {
        return getWidth() / crossLineCount;
    }

    /**
     * Get spacing between horizontal cross lines.
     *
     * @return	The number of pixels between horizontal cross lines.
     */
    private int getHorizontalCrossLineSpacing() {
        return getHeight() / crossLineCount;
    }

    /**
     * Set vertical cross lines. Start from vertical origo line if it exist
     * and proceed in both directions separately if necessary.
     *
     * @param xSpacing	Spacing in between each cross line in pixels.
     */

    private void setVerticalCrossLines(int xSpacing) {
        for (int x = getVerticalOrigoLinePosition();
                                                x < getWidth(); x += xSpacing)
            crossLines.add(new GraphPanelLine(new GraphPanelPoint(x, 0),
                                         new GraphPanelPoint(x, getHeight())));
        for (int x = getVerticalOrigoLinePosition(); x > 0; x -= xSpacing)
            crossLines.add(new GraphPanelLine(new GraphPanelPoint(x, 0),
                                         new GraphPanelPoint(x, getHeight())));
    }
    /**
     * Set horizontal cross lines. Start from horizontal origo line if it
     * exist and proceed in both directions separately if necessary.
     *
     * @param ySpacing	Spacing in between each cross line in pixels.
     */
    private void setHorizontalCrossLines(int ySpacing) {
        for (int y = getHorizontalOrigoLinePosition();
                                                y < getHeight(); y += ySpacing)
            crossLines.add(new GraphPanelLine(new GraphPanelPoint(0, y),
                                        new GraphPanelPoint(getWidth(), y)));
        for (int y = getHorizontalOrigoLinePosition(); y > 0; y -= ySpacing)
            crossLines.add(new GraphPanelLine(new GraphPanelPoint(0, y),
                                        new GraphPanelPoint(getWidth(), y)));
    }

    /**
     * Get position of the vertical origo line. If the line does not exist
     * then return vertical center of the canvas.
     *
     * @return	Position of the vertical origo line.
     */
    private int getVerticalOrigoLinePosition() {
        if (origo.getX() >= 0 && origo.getX() < Integer.MAX_VALUE)
            return origo.getX();
        else
            return getWidth() / 2;
    }
    /**
     * Get position of the horizontal origo line. If the line does not exist
     * then return horizontal center of the canvas.
     *
     * @return	Position of the horizontal origo line.
     */
    private int getHorizontalOrigoLinePosition() {
        if (origo.getY() >= 0 && origo.getY() < Integer.MAX_VALUE)
            return origo.getY();
        else
            return getHeight() / 2;
    }

    /**
     * Set limit text element for minimum value on x axis.
     */
    private void setLimitTextMinX() {
        textLimitMinX.setText(limits.getLimitMinX());
        textLimitMinX.setStyleLimit();
        textLimitMinX.setPosition(DEFAULT_TEXT_SPACING,
                                  getHeight() / 2 - DEFAULT_TEXT_SPACING);
    }

    /**
     * Set limit text element for maximum value on x axis.
     */
    private void setLimitTextMaxX() {
        textLimitMaxX.setText(limits.getLimitMaxX());
        textLimitMaxX.setStyleLimit();
        textLimitMaxX.setPosition(getWidth() - DEFAULT_TEXT_SPACING
                                             - textLimitMaxX.getTextWidth(),
                                  getHeight() / 2 - DEFAULT_TEXT_SPACING);
    }

    /**
     * Set limit text element for minimum value on y axis.
     */
    private void setLimitTextMinY() {
        textLimitMinY.setText(limits.getLimitMinY());
        textLimitMinY.setStyleLimit();
        textLimitMinY.setPosition(getWidth() / 2 + DEFAULT_TEXT_SPACING,
                                  getHeight() - DEFAULT_TEXT_SPACING);
    }

    /**
     * Set limit text element for maximum value on y axis.
     */
    private void setLimitTextMaxY() {
        textLimitMaxY.setText(limits.getLimitMaxY());
        textLimitMaxY.setStyleLimit();
        textLimitMaxY.setPosition(getWidth() / 2 + DEFAULT_TEXT_SPACING,
                                  DEFAULT_TEXT_SPACING + 15);
    }

    /**
     * Set limit text elements to be shown at correct positions and correct
     * values.
     */
    private void setLimitTexts() {
        setLimitTextMinX();
        setLimitTextMaxX();
        setLimitTextMinY();
        setLimitTextMaxY();
    }

    /**
     * Set cursor lines according to cursor point. If cursor point is null then
     * remove cursor lines from display.
     */
    private void setCursorLines() {
        verticalCursorLine.setVisible(false);
        horizontalCursorLine.setVisible(false);
        if (cursor != null) {
            createCursorLines();
            verticalCursorLine.setVisible(true);
            horizontalCursorLine.setVisible(true);
        }
    }

    /**
     * Create new cursor lines.
     */
    private void createCursorLines() {
        verticalCursorLine.setLineStartPoint(cursor.getX(), 0);
        verticalCursorLine.setLineEndPoint(cursor.getX(), getHeight());
        horizontalCursorLine.setLineStartPoint(0, cursor.getY());
        horizontalCursorLine.setLineEndPoint(getWidth(), cursor.getY());
        verticalCursorLine.setStyleCursorLine();
        horizontalCursorLine.setStyleCursorLine();
    }

    /**
     * Update the list containing lines that connect each visible point of a
     * graph. First remove existing lines from the panel. Then loop through
     * each point on the graph and create lines between two points in case both
     * are visible on the panel.
     */
    private void setGraphLines() {
        graphLines.clear();
        if (graph != null)
            for (int index = 0; index < graph.size(); index++) {
                if (index + 1 >= graph.size())
                    break;
                if (isVisible(graph.get(index)) &&
                                                isVisible(graph.get(index + 1)))
                    graphLines.add(createGraphLine(graph.get(index),
                                                   graph.get(index + 1)));
            }
    }

    /**
     * Check if GraphPanelPoint is visible on the panel. To be visible location
     * of the point must be within size limits of the panel.
     *
     * @param point		GraphPoint to be checked for visibility.
     * @return			True if visible and false if not.
     */
    private boolean isVisible(GraphPanelPoint point) {
        if (point == null)
            return false;
        if ((point.getX() >= 0) && (point.getX() <= getWidth()) &&
            (point.getY() >= 0) && (point.getY() <= getHeight()))
            return true;
        return false;
    }

    /**
     * Create a line between two points on a graph.
     *
     * @param start	Start point of the line.
     * @param end	End point of the line.
     */
    private GraphPanelLine createGraphLine(GraphPanelPoint start,
                                           GraphPanelPoint end) {
        GraphPanelLine graphLine = new GraphPanelLine(start, end);
        graphLine.setStyleGraphLine();
        return graphLine;
    }

    /**
     * Update layout parameters for the panel.
     */
    private void setGraphPanelLayoutParams() {
        ViewGroup.LayoutParams params = getLayoutParams();
        params.width = panelWidth;
        params.height = panelHeight;
        setLayoutParams(params);
    }

    /**
     * Draw content of the panel on the given canvas
     *
     * @param canvas    Canvas on which the panel content is to be drawn.
     */
    @Override
    protected void onDraw(Canvas canvas) {
        drawBorder(canvas);
        drawExpressionText(canvas);
        drawOrigoLines(canvas);
        drawCrossLines(canvas);
        drawLimitTexts(canvas);
        drawCursorLines(canvas);
        drawGraphLines(canvas);
        invalidate();
    }

    /**
     * Draw border for the panel.
     *
     * @param canvas    Canvas on which border is to be drawn.
     */
    private void drawBorder(Canvas canvas) {
        border.draw(canvas);
    }

    /**
     * Draw the mathematical expression that represents the graph to the top
     * left corner for user info.
     *
     * @param canvas    Canvas on which expression is to be drawn.
     */
    private void drawExpressionText(Canvas canvas) {
        expressionText.setText(expression);
        expressionText.setPosition(10, 25);
        expressionText.setStyleExpression();
        expressionText.draw(canvas);
    }

    /**
     * Draw origo lines on the panel.
     *
     * @param canvas    Canvas on which the origo lines are to be drawn.
     */
    private void drawOrigoLines(Canvas canvas) {
        setOrigoLines();
        verticalOrigoLine.draw(canvas);
        horizontalOrigoLine.draw(canvas);
    }

    /**
     * Draw cross lines on the panel.
     *
     * @param canvas    Canvas on which the cross lines are to be drawn.
     */
    private void drawCrossLines(Canvas canvas) {
        setCrossLines();
        for (GraphPanelLine line: crossLines) {
            line.draw(canvas);
        }
    }

    /**
     * Draw limits for both axes on to the panel.
     *
     * @param canvas    Canvas on which the limit texts are to be drawn.
     */
    private void drawLimitTexts(Canvas canvas) {
        setLimitTexts();
        textLimitMinX.draw(canvas);
        textLimitMaxX.draw(canvas);
        textLimitMinY.draw(canvas);
        textLimitMaxY.draw(canvas);
    }

    /**
     * Draw cursor lines on to the panel.
     *
     * @param canvas    Canvas on which the cursor lines are to be drawn.
     */
    private void drawCursorLines(Canvas canvas) {
        setCursorLines();
        verticalCursorLine.draw(canvas);
        horizontalCursorLine.draw(canvas);
    }

    /**
     * Draw graph lines on to the panel.
     *
     * @param canvas    Canvas on which the graph lines lines are to be drawn.
     */
    private void drawGraphLines(Canvas canvas) {
        setGraphLines();
        for (GraphPanelLine line : graphLines)
            line.draw(canvas);
    }

    /**
     * Set the mathematical expression. The expression will be shown at the
     * top left corner of the panel for user reference.
     *
     * @param expression Mathematical expression as a string.
     */
    public void setExpression(String expression) {
        this.expression = expression;
    }

    /**
     * Set real value limits of both x and y axes according to the given
     * parameters.
     *
     * @param limits	Real value limits for both x and y axes.
     */

    /**
     * Set real value limits of both x and y axes according to the given
     * parameters.
     *
     * @param minX      Minimum limit for x axis.
     * @param maxX      Maximum limit for x axis.
     * @param minY      Minimum limit for y axis.
     * @param maxY      Maximum limit for y axis.
     */
    public void setLimits(double minX, double maxX, double minY, double maxY) {
        if (limits == null)
            limits = new GraphPanelLimits();
        limits.setLimitsForX(minX, maxX);
        limits.setLimitsForY(minY, maxY);
    }

    /**
     * Set the number of cross lines used. The same value is used for both
     * vertical and horizontal cross lines.
     *
     * @param crossLinesCount   Number of cross lines to be used.
     */
    public void setCrossLinesCount(int crossLinesCount) {
        this.crossLineCount = crossLinesCount;
    }

    /**
     * Set origo to the given (x, y) point. This point determines where the
     * origo lines cross on the panel. If either x or y is set to maximum
     * integer or below 0 then the corresponding origo line, vertical or
     * horizontal, is not shown.
     *
     * @param x     Horizontal coordinate of the origo point. Determines
     *              location of vertical origo line.
     * @param y     Vertical coordinate of the origo point. Determines location
     *              of horizontal origo line.
     */
    public void setOrigo(int x, int y) {
        origo = new GraphPanelPoint(x, y);
    }

    /**
     * Add new point to the list of points representing a graph. Once all
     * points are set call updateGraph method to make the graph visible on the
     * panel.
     *
     * @param x		X coordinate of the point to be added.
     * @param y		Y coordinate of the point to be added.
     */
    public void addGraphPoint(int x, int y) {
        if (graphPointStore == null)
            graphPointStore = new ArrayList<>();
        graphPointStore.add(new GraphPanelPoint(x, y));
    }

    /**
     * Clear list containing all graph points. This allows new graph to be
     * defined for the panel.
     */
    public void clearGraph() {
        if (graphPointStore != null)
            graphPointStore.clear();
        graphPointStore = null;
        graph = null;
    }

    /**
     * Update the graph for panel so that it can be drawn to the panel.
     */
    public void updateGraph() {
        graph = graphPointStore;
    }

    /**
     * Set cursor to the given (x, y) point. This point determines where the
     * cursor lines cross on the panel. If either x or y is set to maximum
     * integer or below 0 then the corresponding cursor line, vertical or
     * horizontal, is not shown.
     *
     * @param x     Horizontal coordinate of the cursor point. Determines
     *              location of vertical cursor line.
     * @param y     Vertical coordinate of the cursor point. Determines location
     *              of horizontal cursor line.
     */
    public void setCursor(int x, int y) {
        if (x < 0 || y < 0 || x == Integer.MAX_VALUE || y == Integer.MAX_VALUE)
            cursor = null;
        else
            cursor = new GraphPanelPoint(x, y);
    }

    /**
     * Update panel size according to parameters. Update layout parameters only
     * if the size has actually changed.
     *
     * @param width     Width for the panel.
     * @param height    Height for the panel.
     */
    public void setPanelSize(int width, int height) {
        if (panelWidth != width || panelHeight != height) {
            panelWidth = width;
            panelHeight = height;
            setGraphPanelLayoutParams();
        }
    }
}
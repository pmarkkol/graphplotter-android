/* GraphPanelLimits.java
 *
 * Copyright (C) 2016 Paavo Markkola <paavo.markkola@iki.fi>
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE.txt file for details.
 */
package fi.markkola.graphplotter_android.graphpanel;

/**
 * Class that represents coordinate limits for a graph.
 *
 * @author Paavo Markkola
 */
public class GraphPanelLimits {

    /**
     * Default maximum and minimum limit constants for both axes.
     */
    private static final double MAXIMUM_LIMIT = 5.0;
    private static final double MINIMUM_LIMIT = -5.0;

    /**
     * Minimum limit for x axis.
     */
    private double limitMinX;

    /**
     * Maximum limit for x axis.
     */
    private double limitMaxX;

    /**
     * Minimum limit for y axis.
     */
    private double limitMinY;

    /**
     * Maximum limit for y axis.
     */
    private double limitMaxY;

    /**
     * Default constructor for GraphPanelLimits.
     */
    public GraphPanelLimits() {
        setDefaultLimits();
    }

    /**
     * Set default limits for both axes.
     */
    public void setDefaultLimits() {
        setDefaultLimitsForX();
        setDefaultLimitsForY();
    }

    /**
     * Set default limits for the x axis.
     */
    public void setDefaultLimitsForX() {
        limitMinX = MINIMUM_LIMIT;
        limitMaxX = MAXIMUM_LIMIT;
    }

    /**
     * Set default limits for the y axis.
     */
    public void setDefaultLimitsForY() {
        limitMinY = MINIMUM_LIMIT;
        limitMaxY = MAXIMUM_LIMIT;
    }

    /**
     * Set limits for the x axis.
     *
     * @param limitMinX		Minimum limit for x.
     * @param limitMaxX		Maximum limit for x.
     */
    public void setLimitsForX(double limitMinX, double limitMaxX) {
        if (limitMinX >= limitMaxX)
            return;
        this.limitMinX = limitMinX;
        this.limitMaxX = limitMaxX;
    }

    /**
     * Set limits for the y axis.
     *
     * @param limitMinY		Minimum limit for y.
     * @param limitMaxY		Maximum limit for y.
     */
    public void setLimitsForY(double limitMinY, double limitMaxY) {
        if (limitMinY >= limitMaxY)
            return;
        this.limitMinY = limitMinY;
        this.limitMaxY = limitMaxY;
    }

    /**
     * Get minimum limit for the x axis.
     *
     * @return	Minimum limit for x.
     */
    public double getLimitMinX() {
        return limitMinX;
    }

    /**
     * Set minimum limit for the x axis.
     *
     * @param limitMinX		Minimum limit for x.
     */
    public void setLimitMinX(double limitMinX) {
        this.limitMinX = limitMinX;
    }

    /**
     * Get maximum limit for the x axis.
     *
     * @return	Maximum limit for x.
     */
    public double getLimitMaxX() {
        return limitMaxX;
    }

    /**
     * Set maximum limit for the x axis.
     *
     * @param limitMaxX		Maximum limit for x.
     */
    public void setLimitMaxX(double limitMaxX) {
        this.limitMaxX = limitMaxX;
    }

    /**
     * Get minimum limit for the y axis.
     *
     * @return	Minimum limit for y.
     */
    public double getLimitMinY() {
        return limitMinY;
    }

    /**
     * Set minimum limit for the y axis.
     *
     * @param limitMinY		Minimum limit for y.
     */
    public void setLimitMinY(double limitMinY) {
        this.limitMinY = limitMinY;
    }

    /**
     * Get maximum limit for the y axis.
     *
     * @return	Maximum limit for y.
     */
    public double getLimitMaxY() {
        return limitMaxY;
    }

    /**
     * Set maximum limit for the y axis.
     *
     * @param limitMaxY		Maximum limit for y.
     */
    public void setLimitMaxY(double limitMaxY) {
        this.limitMaxY = limitMaxY;
    }
}

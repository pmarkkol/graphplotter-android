/* GraphPanelText.java
 *
 * Copyright (C) 2016 Paavo Markkola <paavo.markkola@iki.fi>
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE.txt file for details.
 */
package fi.markkola.graphplotter_android.graphpanel;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;

/**
 * Class that prints text in different styles to a graph panel.
 *
 * @author Paavo Markkola
 */
public class GraphPanelText {

    private String text;
    private int positionX;
    private int positionY;
    private Paint textPaint;

    public GraphPanelText() {
        textPaint = new Paint();
        setPosition(0, 0);
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setText(double limit) {
        this.text = Double.toString(limit);
    }

    public void setPosition(int x, int y) {
        positionX = x;
        positionY = y;
    }

    public int getTextWidth() {
        return (int) textPaint.measureText(text);
    }

    public void setStyleExpression() {
        textPaint.setColor(Color.BLUE);
        textPaint.setTextAlign(Paint.Align.LEFT);
        textPaint.setTextSize(20);
        textPaint.setTypeface(Typeface.create("monospace", Typeface.BOLD));
    }

    public void setStyleLimit() {
        textPaint.setColor(Color.BLACK);
        textPaint.setTextAlign(Paint.Align.LEFT);
        textPaint.setTextSize(15);
        textPaint.setTypeface(Typeface.create("monospace", Typeface.BOLD));
    }

    public void draw(Canvas canvas) {
        if (text != null)
            canvas.drawText(text, positionX, positionY, textPaint);
    }
}

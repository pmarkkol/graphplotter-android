# Graphplotter-Android #

Android application of [Calculator](https://bitbucket.org/pmarkkol/calculator) project.

### Requirements ###

 - Java Platform, Standard Edition 8. See http://www.oracle.com/technetwork/java/javase/overview/index.html for install instructions.
 - git. See https://git-scm.com/ for more info.
 - Android Studio 


### Instructions ####

Make sure that Java and git are properly installed. To verify run commands

	javac -version 
	git --version

In case of error check the install instructions for these software. 

Checkout Graphplotter-Android source from repository with command:

	git clone https://pmarkkol@bitbucket.org/pmarkkol/graphplotter-android.git

Launch Android Studio and open the project. 


### Contact ###

Paavo Markkola (paavo.markkola@iki.fi)